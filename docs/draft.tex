\documentclass[12pt, a4paper]{report}
\usepackage{graphicx}
\graphicspath{ {/home/joemonk/screenshots/} }
\usepackage{float}
\usepackage{listings}
\usepackage{mathptmx}
\usepackage{geometry}
\geometry{
	top=1in,
	bottom=1in,
	left=1in,
	right=1in
}
\linespread{1.3}

\title{TSP - The Travelling Salesman Problem}
\author{
A dissertation submitted in partial fulfilment of
\\
the requirements for the degree of
\\
Bachelor of Engineering in Computer Science
\\
in
\\
The Queen's University of Belfast
\\
by
\\
Matthew Rooney}
\date{12th May 2015}

\begin{document}
\maketitle

\begin{center}\textbf{Abstract}
\\
The following investigation looks at the well-known combinatorial optimisation problem, The Travelling Salesman problem. It seeks to find the best algorithms for calulating tours with the shortest path as well as a short amount of time.
A workbench was created to help the user walk through the investigation and to understand the algorithms.
In conclusion, the best algorithms to fit this purpose are approximations, notably Twice-around-the-tree.
\end{center}

\tableofcontents

\chapter{Introduction}
\section{The Problem}
   `Given a set of cities along with the cost of travel between each pair of them, the traveling salesman problem, or TSP for short, is to find the cheapest way of visiting all the cities and returning to the starting point. The ``way of visiting all the cities" is simply the order in which the cities are visited; the ordering is called a tour or circuit through the cities.'[1]
\\\\
   The Travelling Salesman is a problem in combinatorial optimisation that was first postulated in the 1800s. It asks the question: which is the least cost tour in which each city is visited exactly once? This problem has long interested mathematicians and is very significant in theoretical computer science. Ideally, salesmen making round trips do so in a way that involves the least cost (or the least time). Although solutions for TSP were proposed before the advent of computers, finding an optimal tour was infeasible for problems with more than just a few cities. This is because the number of possible tours for a problem with n cities is (n-1)! Thus, a problem with 5 cities will have 24 possible routes whereas a problem with 10 cities will have 362,880. Dividing this number by 2 will give the number of unique paths, although this assumes that the cost of a journey will be the same in either direction (in Euclidean space), which is not always true. One simple example of a reversed tour being of a different cost would be a journey up and down a large hill; the cost associated with travelling uphill would likely be more than the cost travelling downhill.
\\\\
As the Travelling Salesman is a well-documented area of computer science, there exists a large amount of descriptive data for those problems that have been solved for the optimal tour. The files containing this data and other unsolved problems can be found at the TSPLib website.[2] This data has been collected by many different TSP theorists and will prove to be invaluable to this investigation.
\\\\
   One noteworthy dataset for the Travelling Salesman is the United States problem which contains 49 cities. An optimum solution for this problem was found by Dantzig, Fulkerson and Johnson in 1954. As of 2006, the largest problem for which the optimum tour has been found has 85,900 cities.

\subsection{Real-world applications of TSP}
\textbf{Printed Circuit Boards (Drilling of holes in PCBs)}\\
	In the manufacture of printed circuit boards, many thousands of holes may need to be drilled to provide conduits for electronic signals whilst saving the optimum amount of space. This process is normally automated and involves large scale problems. The use of a suitable TSP algorithm to find the shortest path for a drill could considerably reduce production time. This is a crucial saving in the business of manufacturing computers and other electronic equipment.
	
\begin{figure}[H]
	\includegraphics[width=9cm]{drill}
		\centering
		\caption{Automated PCB drilling}
\end{figure}

\noindent\textbf{Parcel delivery}\\
	A parcel delivery service may make use of TSP algorithms to plot routes and schedule delivery vans. Rather than putting all of the destinations into a single problem, multiple smaller sub-problems can be devised to help decide the number of vehicles to send out for delivery. Many notable companies use the Travelling Salesman Problem to help with this decision every day. This can be crucial for various reasons, e.g. fuel and time saving.


\section{The Solution}

\subsection{Euclidean Graphs}
A graph G consists of a list of vertices V and a list of edges E.\\
G (V, E)\\
Each edge has a start vertex u, an end vertex u and a weight (or cost) u associated with it.\\
E (u, v \textbar\ w)\\
For Euclidean graphs, the edge E (u, v) and the edge E (v, u) have the same cost w.


\subsection{Optimal Algorithms}
	A number of algorithms have been proposed for the Travelling Salesman Problem. Depending on the nature and size of the problem, there are varying degrees of effectiveness.
\\\\
\textbf{Exhaustive search}\\
The exhaustive search algorithm (brute force approach) examines all possible tours and picks the cheapest one. This algorithm works well with small problems. For larger problems the number of possible tours grows exponentially and so the amount of computation needed may become infeasible. One goal of this project is to determine the sizes of problems for which exhaustive search is effective.

\subsection{Approximation Algorithms}
For larger problems, it becomes apparent that an approximation algorithm is the better choice for generating a tour. It is important to note that, unlike exhaustive search, these algorithms may not compute an optimum solution. Rather, they will produce estimations with varying levels of efficiency and some algorithms will be able to guarantee a worst case for tour cost.
\\\\
\noindent\textbf{Nearest Neighbour}\\
Nearest Neighbour finds a tour that may not be optimal. From a start city, it chooses to visit the next closest city. This strategy is repeated until no more cities remain unvisited; and the tour is completed by returning to the start city. One potential weakness with this algorithm is that at the end of the nearest neighbour process there might be a large cost associated with completing the tour.
\\\\
	There are two other, well-known algorithms that have bounds placed on the size of tour that they return. \textbf{Twice-around-the-tree} returns a tour that is no worse than twice the optimal tour; \textbf{Christofides' Algorithm} returns a tour that is no worse than 1.5 times the optimal tour. Both of these algorithms use a technique of first finding the minimum spanning tree. There will be further detail on the algorithms later in the report.
The aim of the project is to determine the effectiveness of these approximation algorithms for solving problems with 50-200 nodes.

\subsection{Workbench}
   Abstractly, the cities and interconnections in TSP are represented by means of a graph. A graph is a collection of points (vertices) and a collection of connections, possibly with costs (edges with weights). An example graph with connections and costs is displayed below, with the optimum tour highlighted:

\begin{figure}[H]
	\includegraphics[width=9cm]{graph2}
		\centering
		\caption{Example graph with solution}
\end{figure}

\noindent Pictorial examples of large problems are cluttered and hard to read. The aim of the project is to create a workbench that can be used to investigate the effectiveness of the four algorithms introduced earlier. The workbench should have the capability to consume graph data from the TSPLib website. The workbench should be able to apply the four algorithms to compute the costs of the tours that they generate. For example, there will be a demonstration of how large a problem can be solved with exhaustive search. The effectiveness of each of the algorithms can be determined by comparing, for a range of graphs, the ratio of the actual solution found against the cost of the optimum tour.

\subsection{Experimental Evaluation}
   The effectiveness of each approximation algorithm can be determined by comparing, for a range of graphs, the ratio of the cost of the solution found by the algorithm against the cost of the optimum tour. The algorithms will be ranked from `best' to `worst' for the graph data used in the investigation, determining the most suitable algorithm for dealing within this range of problem sizes.

\subsection{Requirements}
\begin{itemize}
	\item Develop a workbench for processing ``large graphs"

	\item The workbench should be able to use data extracted from the TSPLib website

	\item The workbench will allow the following algorithms to be executed:\\
     	- exhaustive search\\
     	- ``shortest" approximation\\
     	- Twice-around-the-tree ( \textless= 2 times the optimum )\\
     	- Christofides' algorithm ( \textless= 1.5 times the optimum )

	\item Carry out experiments for a range of graphs to determine the effectiveness of each of the algorithms above
\end{itemize}

\chapter{TSP Algorithms}
Solutions for the Travelling Salesman Problem can be generated by the following algorithms for this investigation:

\section{Exhaustive Search}
Exhaustive search is the name given to any algorithm that analyses each and every possible tour before choosing the best one. This is an efficient way to guarantee the optimal tour is given for small graphs. However, problems arise when the size becomes more than just a few cities; the time taken to complete the search increases exponentially. When considering problems with a large number of cities, it becomes clear that an approximation must be used.

\section{Approximation Algorithms}
An approximation algorithm can be used to generate a suggested best tour, although there will be no guarantee that it will be the optimal tour. There are many algorithms that can ensure that the tour given will have a worst case upper bound and this can provide a approximate solution for very large problems that would otherwise be unsolvable. A selection of some of the most popular approximation algorithms are detailed below.

\subsection{Nearest Neighbour}
Nearest Neighbour begins by choosing an arbitrary starting node. The algorithm iteratively adds one node at a time, using a greedy process until all nodes are present. At each stage the edge of least weight emanating from the current node and going to an unvisited node is added. This node is removed from consideration and this process is repeated until all of the nodes have been visited. With this algorithm it is a possibility that, upon reaching the final node in the tour, the path to the starting node has a considerable cost associated with it. It is a greedy algorithm that in its worst case generates a tour much larger than the optimal tour.

\begin{figure}[H]
	\includegraphics[width=9cm]{graph1}
		\centering
		\caption{Nearest Neighbour tour: A -\textgreater\ C -\textgreater\ B -\textgreater\ E -\textgreater\ D -\textgreater\ A}
\end{figure}

\subsection{Twice-around-the-tree}
Some algorithms make use of a minimum spanning tree as a basis upon which to run further optimisations. A minimum spanning tree encompasses all nodes within the problem but is not a complete circuit. The method for generating the tree begins much like Nearest Neighbour in that a starting node is chosen first of all and the edge with the least cost is chosen next. After this initial step, another lowest cost node must be chosen that is both unvisited and connected to any of the previously visited nodes. If more than one qualifying node shares this lowest cost with another, a node is chosen arbitrarily from them. This process is repeated until each node is included in the tree.

\begin{figure}[H]
	\includegraphics[width=9cm]{graph4}
		\centering
		\caption{Minimum spanning tree}
\end{figure}

The next part of this algorithm consists of a walk of the minimum spanning tree. This means that the starting node is treated as the root of the tree and a path is made through all of the tree's nodes. Using a depth-first approach, the value of the next node is added to the tour and any node with more than one path is taken branch-by-branch until the current branch comes to an end. The path is then retraced backwards to the node with multiple paths and the next, unvisited branch is followed, remembering to add to the tour only the value of nodes that are being visited for the first time, creating a shortcut. This tour without repeated nodes is the result of twice-around-the-tree and has a guaranteed, worst case cost equalling twice that of the optimal tour.

\subsection{Christofides' Algorithm}
For Christofides' algorithm, again a minimum spanning tree is generated. However, this time there is a more systematic approach to arriving at an approximation for the optimal tour. A subset of nodes from the minimum spanning tree is generated selecting only those nodes with odd degree. That is to say, all nodes with an odd number of outstretching edges.

\begin{figure}[H]
	\includegraphics[width=9cm]{graph5}
		\centering
		\caption{Nodes with odd degree in red}
\end{figure}

This ensures that the resulting set is of even cardinality. It is this property which allows for the execution of the next step in the process, minimal weight perfect matching. This is the process of joining each node with exactly one other node to obtain a list of isolated edges with minimum weight. In this case, where time and processing power is a factor, it is sufficent to use an approximation of perfect matching.

\begin{figure}[H]
	\includegraphics[width=9cm]{graph6}
		\centering
		\caption{Odd degree nodes with perfect matching}
\end{figure}

Once all odd degree nodes have been paired with exactly one other node, they keep their odd degree and form a set of non-adjacent edges. These are then united with the minimum spanning tree from before to comprise a multigraph which contains only even degree nodes.

\begin{figure}[H]
	\includegraphics[width=9cm]{graph7}
		\centering
		\caption{Multigraph of all even degree nodes}
\end{figure}

Having all nodes of even degree makes the graph Eulerian and as such, can be traced completely without using the same edge twice. This Eulerian tour is calculated, using shortcutting to avoid visiting nodes twice, to give the Christofides tour.\\\\
e.g. A -\textgreater\ B -\textgreater\ C -\textgreater\ D -\textgreater\ E -\textgreater\ C -\textgreater\ A \\ \indent\indent becomes
\\
\indent\ A -\textgreater\ B -\textgreater\ C -\textgreater\ D -\textgreater\ E -\textgreater\ A


\section{Design}
\subsection{Data Input}
All of the input data for graphs is taken from TSPLib and is stored within plain text files (with some description of problem in preamble). For the purposes of this investigation, only Euclidean graphs will be examined. Those files have data line by line in the following format:
\\\\
\textless Index of node\textgreater \ \ \textless X co-ordinate\textgreater \ \ \textless Y co-ordinate\textgreater
\\\\
Due the data being stored in this way, it is not difficult to calculate costs for these files and store them in an adjacency matrix. As we will know the size of the graph, this data can be stored in a 2-dimensional array which will allow faster calculation of tours. Some thought must be given to the values of the co-ordinates however, as they are presented in a few different formats (including scientific form). There are also some graphs with a very large number of nodes which will generate a substantial matrix.

\subsection{Data manipulation}
The data which has been read in from the TSPLib files will be processed into an adjacency matrix and then the aforementioned algortihms will be utilised to generate different solutions for each problem. Depending on the problem in question, there may be restrictions due to the number of nodes. Thus, it is wise to have a rough idea of the computational cost before running these algorithms for a graph.
\\\\
During the manipulation of the data it is important to have a quick way of referring to each item and its corresponding properties. Therefore, the nodes will be referenced using their index from the source files.

\subsection{Data presentation}
Having generated a solution for a graph using one of the algortihms, it is imperative that this information is displayed in a manner that is easily understandable and succinct. With this in mind, the output tour will be presented in a 3-columned table much like that of the source (with index, x-coord \& y-coord) but with the data rearranged in the order of the resulting soluton.
\\\\
As well as the output tour, it is relevant to have values for the tour cost, and time taken for execution. This will also be displayed in table form along with the name of the algorithms performed so that it is ready for comparison.

\chapter{Implementation} %IMPLEMENTATION%

\textbf{Coordinate class}\\
An abstract 2-tuple class created to hold data both for graph coordinates when loading in TSPLib files, and graph edges when calculating tours based on those edges.
\\
\textbf{Workbench}\\
A simple swing GUI to allow the user to test different algorithms on the selected graph and display human-readable results on screen.
\\
\textbf{Graph class}\\
A class to hold and manipulate edges (Coordinates), used in the construction of the Minimum Spanning Tree.
\\
\textbf{EulerianCircuit}\\
A means of generating a complete circuit through the nodes, touching each edge exactly once. Essential for Christofides' Algorithm.
\\
\textbf{TSPGraph}\\
The object which holds all of the data for a particular problem and the algorithms to be executed on it.
\\
\textbf{get\textless Algortihm\textgreater Tour()}\\
Algorithm methods that generate tours as well as noting the time taken to complete the operation.
\\
\textbf{createMatrix()}\\
Method for initialising a matrix of edges and their associated weights.
\\
\textbf{findTourLength()}\\
Returns the cost associated with a given list of nodes by consulting the weight matrix


\chapter{Conclusions} %CONCLUSION%

\section{Experimental results}
For small graphs of only a few nodes, we see that the exhaustive search is consistently the best performing in terms of cost and time. However, it is unfeasible to use exhaustive search for larger problems. The time taken to generate a tour is growing at an exceptional rate as displayed below:
\\\\
\begin{tabular}{|c|c|c|c|c|}
\hline
	Algorithm & Problem Size & Cost & Time (s) & Ratio of Opt\\
\hline
	Exhaustive Search & 5 & 106.991 & 0.001 & 1\\
\hline
	Nearest Neighbour & 5 & 115.182 & 0.000 & 1.077\\
\hline
	Twice-around-the-tree & 5 & 122.678 & 0.002 & 1.147\\
\hline
\hline
\hline
	Exhaustive Search & 15 & 208.009 & 21.513 & 1\\
\hline
	Nearest Neighbour & 15 & 262.233 & 0.000 & 1.261\\
\hline
	Twice-around-the-tree & 15 & 277.861 & 0.002 & 1.336\\
\hline
\hline
\hline
	Exhaustive Search & 16 & 213.203 & 132.013 & 1\\
\hline
	Nearest Neighbour & 16 & 266.063 & 0.000 & 1.248\\
\hline
	Twice-around-the-tree & 16 & 315.641 & 0.006 & 1.481\\
\hline
\hline
\hline
	Nearest Neighbour & 51 & 513.610 & 0.001 & 1.206\\
\hline
	Twice-around-the-tree & 51 & 600.367 & 0.012 & 1.409\\
\hline
\hline
\hline
	Nearest Neighbour & 657 & 62176.401 & 0.036 & 1.271\\
\hline
	Twice-around-the-tree & 657 & 104168.213 & 3.108 & 1.967\\
\hline
\hline
\hline
	Nearest Neighbour & 1002 & 963129.31 & 1.032 & 3.718\\
\hline
	Twice-around-the-tree & 1002 & 505137.75 & 25.133 & 1.950\\
\hline
\end{tabular}
\\\\
Nearest Neighbour performs the quickest in every situation but can quickly increase in cost due to the non-existence of bounds. The Nearest Neighbour algorithm was run 3 times with different starting points and the best tour was chosen. this explains the preferencial ratios in most cases.
Twice-around-the-tree takes a little longer but can guarantee a worst case for the cost and is therefore the best choice for the largest problem sizes.


\section{Suggested improvements}
Unfortunately, there was not enough time to correct issues with the implementation of Christofides' Algorithm and as a result, the data generated was proven to be unbounded for large graphs, as is shown in the appendix. It would be beneficial to this study of TSP if the algorithm were perfected and executed to yield useful, cost-bounded tours.
\\\\
Another possible improvement for this investigation would be to include non-Euclidean graphs. It would be worthwhile to examine the implications of using edges in a directed graph. TSPLib contains non-Euclidean graph data and could be utilised to aid in the study.

\chapter{References}
%LIST REFERENCES%
[1] David L. Applegate, Robert E. Bixby, Vasek Chvátal \& William J. Cook: The Traveling Salesman Problem: A Computational Study
\\\\
\noindent[2] TSPLib: http://www.iwr.uni-heidelberg.de/groups/comopt/software/TSPLIB95/
\\\\
\noindent[3] David S. Johnson \& Lyle A. McGeoch: The Traveling Salesman Problem: A Case Study in Local Optimization
\\\\
\noindent[4] Christofides' Heuristic: http://ieor.berkeley.edu/~kaminsky/ieor251/notes/2-16-05.pdf
\\\\
\noindent[5] N. Christofides: Worst-case analysis of a new heuristic for the travelling salesman problem

\chapter{Appendices}

\section{Workbench User Manual}
The  workbench allows us to take a closer look at the algorithms in question and gives us a platform to compare efficiency and the logistics of each algorithm relative to the problem size.
\\\\
Upon loading the application, the user is presented with a window separated into two partitions. On the left, there are three options: Load small graph, load large graph, and quit.

\begin{figure}[H]
	\includegraphics[width=12cm]{gui1}
		\centering
		\caption{Workbench start}
\end{figure}

Choosing to load graphs opens a file chooser window in the respective directory to allow TSPLib data to be read into the workbench for investigation (and `Quit' exits the application). Large graphs are characterised as those with sizes greater than a few hundred and as such, should be expected to take longer to process. Once a TSPLib file has been selected, a scrollable table is loaded which displays a list of the x and y coordinates for the chosen graph along with its corresponding index. Also appearing are four new buttons with the names of the algorithms that are to be tested. Notice, that for some of the graphs (size \textgreater\ 16), the `Exhuastive Search' button has been disabled, indicating that the time to execute this algorithm is beyond the purposes of the workbench.

\begin{figure}[H]
	\includegraphics[width=12cm]{gui2}
		\centering
		\caption{Choosing a graph file}
\end{figure}

\begin{figure}[H]
	\includegraphics[width=12cm]{gui3}
		\centering
		\caption{Loaded graph}
\end{figure}

The right side of the window, which was previously blank, now has a table with empty fields for displaying data generated via the algorithms on the left panel. The table columns are: Algorithm, Tour cost, Time taken and Ratio of optimal. Clicking one of the algorithm buttons starts a process that calculates a tour using that method and when it is complete, updates the table with the results. There will also be a text area containing the resulting order of nodes for the current algortihm. Opening a new graph will clear the result table and update the window with the newly selected coordinates.

\begin{figure}[H]
	\includegraphics[width=12cm]{gui6}
		\centering
		\caption{Graph with completed fields}
\end{figure}

\section{Generated data}

\subsection{Exhaustive Search}

\begin{center}\begin{tabular}{|c|c|c|c|}
\hline
	Problem Size & Cost & Time Taken (s) & Ratio to Optimum\\
\hline
	5 & 106.991 & 0.001 & 1\\
\hline
	6 & 113.713 & 0.003 & 1\\
\hline
	7 & 136.268 & 0.013 & 1\\
\hline
	8 & 139.140 & 0.030 & 1\\
\hline
	9 & 146.764 & 0.065 & 1\\
\hline
	10 & 160.649 & 0.092 & 1\\
\hline
	11 & 167.807 & 0.128 & 1\\
\hline
	12 & 169.160 & 0.294 & 1\\
\hline
	13 & 190.373 & 1.171 & 1\\
\hline
	14 & 191.850 & 3.374 & 1\\
\hline
	15 & 208.009 & 21.513 & 1\\
\hline
	16 & 213.203 & 132.013 & 1\\
\hline
\end{tabular}\end{center}

\subsection{Nearest Neighbour}
\begin{center}\begin{tabular}{|c|c|c|c|}
\hline
	Problem Size & Cost & Time Taken (s) & Ratio to Optimum\\
\hline
	5 & 115.182 & 0.000 & 1.077\\
\hline
	6 & 126.6452 & 0.000 & 1.114\\
\hline
	7 & 142.797 & 0.000 & 1.048\\
\hline
	8 & 139.140 & 0.000 & 1\\
\hline
	9 & 146.764 & 0.000 & 1\\
\hline
	10 & 170.599 & 0.000 & 1.062\\
\hline
	11 & 197.774 & 0.000 & 1.179\\
\hline
	12 & 212.052 & 0.000 & 1.254\\
\hline
	13 & 237.889 & 0.001 & 1.250\\
\hline
	14 & 260.315 & 0.000 & 1.357\\
\hline
	15 & 262.233 & 0.000 & 1.261\\
\hline
	16 & 266.063 & 0.000 & 1.248\\
\hline
	51 & 513.610 & 0.001 & 1.206\\
\hline
	76 & 153461.923 & 0.001 & 1.419\\
\hline
	280 & 2539.792 & 0.0224 & 1.015\\
\hline
	493 & 43646.376 & 0.0228 & 1.247\\
\hline
	657 & 62176.401 & 0.036 & 1.271\\
\hline
\end{tabular}\end{center}

\subsection{Twice-around-the-tree}
\begin{center}\begin{tabular}{|c|c|c|c|}
\hline
	Problem Size & Cost & Time Taken (s) & Ratio to Optimum\\
\hline
	5 & 122.678 & 0.002 & 1.147\\
\hline
	6 & 155.143 & 0.000 & 1.364\\
\hline
	7 & 178.492 & 0.000 & 1.310\\
\hline
	8 & 171.854 & 0.001 & 1.235\\
\hline
	9 & 179.479 & 0.001 & 1.223\\
\hline
	10 & 204.212 & 0.002 & 1.271\\
\hline
	11 & 200.212 & 0.001 & 1.193\\
\hline
	12 & 203.726 & 0.001 & 1.204\\
\hline
	13 & 231.866 & 0.002 & 1.218\\
\hline
	14 & 252.325 & 0.002 & 1.315\\
\hline
	15 & 277.861 & 0.002 & 1.336\\
\hline
	16 & 315.641 & 0.006 & 1.481\\
\hline
	51 & 600.367 & 0.012 & 1.409\\
\hline
	76 & 148910.702 & 0.017 & 1.377\\
\hline
	280 & 4266.967 & 0.392 & 1.655\\
\hline
	493 & 65651.608 & 1.115 & 1.876\\
\hline
	657 & 104168.213 & 3.108 & 1.967\\
\hline
\end{tabular}
\end{center}
\subsection{Christofides' Algorithm}
\begin{center}\begin{tabular}{|c|c|c|c|}
\hline
	Problem Size & Cost & Time Taken (s) & Ratio to Optimum\\
\hline
	5 & 106.991 & 0.017 & 1\\
\hline
	6 & 155.143 & 0.001 & 1.364\\
\hline
	7 & 178.492 & 0.004 & 1.310\\
\hline
	8 & 180.570 & 0.005 & 1.298\\
\hline
	9 & 188.194 & 0.002 & 1.282\\
\hline
	10 & 170.599 & 0.005 & 1.062\\
\hline
	11 & 192.509 & 0.005 & 1.147\\
\hline
	12 & 214.880 & 0.006 & 1.270\\
\hline
	13 & 246.626 & 0.006 & 1.296\\
\hline
	14 & 259.537 & 0.008 & 1.353\\
\hline
	15 & 294.7314 & 0.008 & 1.417\\
\hline
	16 & 296.817 & 0.004 & 1.392\\
\hline
	51 & 771.862 & 0.020 & 1.812\\
\hline
	76 & 192905.681 & 0.024 & 1.784\\
\hline
	280 & 8019.684 & 0.373 & 3.110\\
\hline
	493 & 142840.692 & 1.075 & 4.081\\
\hline
	657 & 207281.156 & 2.924 & 4.238\\
\hline
\end{tabular}
\end{center}


\section{Java Code}
\lstset{
basicstyle=\tiny
}
\subsection{Coordinate.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/Coordinate.java}
\subsection{Graph.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/Graph.java}
\subsection{TSPGraph.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/TSPGraph.java}
\subsection{TuiScriptable.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/TuiScriptable.java}
\subsection{TreeNode.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/TreeNode.java}
\subsection{Workbench.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/Workbench.java}
\subsection{TSP.java}
\lstinputlisting[language=Java]{/home/joemonk/uni/tsp/java/tsp/src/main/java/Salesman/TSP.java}
\end{document}