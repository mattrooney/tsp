CS Project - Travelling Salesman Problem

This repository is the programming side of the TSP CS project.
The aim is to create a workbench interface where TSPlib graph data can be read in, have different algorithms applied to it, and show a solution with the distance from the optimum.