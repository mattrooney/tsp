/*
 *  This is a quick TreeNode class for use with MST and Twice-around-the-tree
 */
package Salesman;

import java.util.ArrayList;

public class TreeNode<T> {

    T value;
    TreeNode<T> parentNode;
    ArrayList<TreeNode<T>> childNodes;
    boolean isRoot;
    boolean isOdd;

    // Node constructor
    public TreeNode(T value) {
        this.value = value;
        this.childNodes = new ArrayList<TreeNode<T>>();
        this.isRoot = false;
    }

    public TreeNode(T value, boolean root) {
        this.value = value;
        this.childNodes = new ArrayList<TreeNode<T>>();
        this.isRoot = root;
    }

    public boolean checkRoot() {
        return this.isRoot;
    }

    // Add a child to the current node
    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<T>(child);
        childNode.parentNode = this;
        this.childNodes.add(childNode);
        return childNode;
    }

    public void depthFirstPrint() {
        TreeNode<T> currNode = this;
        if (currNode.checkRoot())
            System.out.println(currNode.value + " degree = " + getDegree());
        for (TreeNode<T> child : currNode.childNodes) {
            System.out.println(child.value + " degree = " + getDegree());
            child.depthFirstPrint();
        }
    }

    public TreeNode<T> findNode(T value) {
        TreeNode<T> currNode = this;
        for (TreeNode<T> child : currNode.childNodes) {
            if (child.value == value) {
                currNode = child;
                break;
            }
            else
                if (child.findNode(value).value == value)
                    currNode = child;
        }
        return currNode;
    }

    public int getDegree() {
        if (this.isRoot) {
            if (this.childNodes.isEmpty())
                return 0;
            else
                return this.childNodes.size();
        }
        else if (this.childNodes.isEmpty())
            return 1;
        else
            return (this.childNodes.size() + 1);
    }

}
