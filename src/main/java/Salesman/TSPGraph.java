package Salesman;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.io.*;

public class TSPGraph {
    private ArrayList<Coordinate<Double,Double>> list;
    private ArrayList<Coordinate<Integer,Integer>> MST;
    private ArrayList<Coordinate<Integer,Integer>> pointsUsed;
    private ArrayList<Integer> optimalTour;
    private ArrayList<Integer> testTour;
    private ArrayList<Integer> nnTour;
    private ArrayList<Integer> twiceTour;
    private ArrayList<Integer> oddDegrees;
    private ArrayList<Integer> chrTour;
    private double optimalTourLength;
    private double nnTourLength;
    private double treeTourLength;
    private double twiceTourLength;
    private double chrTourLength;
    private double timeTaken;
    private double[][] adjMatrix;
    private String name;
    private String filename;
    private int startCity = 1;
    private TreeNode<Integer> minimumSpanningTree;

    // TSPGraph constructor
    public TSPGraph(File file) {
        this.list = readGraphData(file);
        this.testTour = new ArrayList<Integer>();
        this.testTour.add(this.startCity);
        this.optimalTourLength = Integer.MAX_VALUE;
    }

    // Restore test tour to the original state
    public void resetTestTour() {
        this.testTour = new ArrayList<Integer>();
        this.testTour.add(this.startCity);
    }

    // Read in graph data from file
    public ArrayList<Coordinate<Double,Double>> readGraphData(File file) {
        File f = file;
        BufferedReader br = null;
        ArrayList<Coordinate<Double,Double>> al = new ArrayList<Coordinate<Double,Double>>();
        String line;
        String[] lineArray = new String[3];
        try {
            br = new BufferedReader(new FileReader(file));
            // Skip the info at the beginning of file
            for(int i=0;i<6;i++) {
				line = br.readLine();
                if (i == 0) {
					this.filename = line.split(" ")[line.split(" ").length - 1];
                    if (this.filename.contains("\\."))
                        this.filename = this.filename.split("\\.")[0];
				}
                if (line.startsWith("COMMENT")) {
					this.name = line.split(": ")[1];
				}
            }
            while ((line = br.readLine()) != null) {
                // Split into Node, X, Y
                if (line.matches(".*\\d+\\s.*\\d+.*")) {
                    lineArray = line.split("\\s+");
                    al.add(new Coordinate<Double,Double>(Double.parseDouble(lineArray[1].replace("+","")),Double.parseDouble(lineArray[2].replace("+",""))));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return al;
    }

    // Get coordinates as String[][] for a graph in table form
    public String[][] getCoords() {
		int node = 0;
		String[][] vertices = new String[this.list.size()][3];
        for (Coordinate<Double,Double> c : this.list) {
            vertices[node][0] = String.valueOf(node + 1);
			vertices[node][1] = String.valueOf(c.x);
			vertices[node][2] = String.valueOf(c.y);
			++node;
		}
        return vertices;
    }

    // Print coordinates for a graph in table form
    public void printCoords() {
        int node = 0;
        System.out.println("     X | Y");
        for (Coordinate<Double,Double> c : this.list) {
            ++node;
            System.out.println(node + " = (" + c.x + "," + c.y + ")");
        }
    }

    // Create an adjacency matrix from list of graph data
    public void createMatrix() {
        this.adjMatrix = new double[this.list.size()][this.list.size()];
        for(int row=0;row < this.adjMatrix.length;row++) {
            for(int cell=0;cell < this.adjMatrix[row].length;cell++) {
                this.adjMatrix[cell][row] = euclideanDistance(list.get(cell),list.get(row));
            }
        }
    }

    // Output adjacency matrix to file
    public void printMatrix(String fileOut) {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new File(fileOut), "UTF-8");
            for(double[] row : this.adjMatrix) {
                for(double cell : row) {
                    writer.printf("%.2f ", cell);
                }
                writer.println();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            writer.close();
        }
    }

    // Find Euclidean distance between 2 Coordinates
    public double euclideanDistance(Coordinate<Double,Double> pointA,Coordinate<Double,Double> pointB) {
        double dx = (Math.abs(pointB.x - pointA.x));
        double dy = (Math.abs(pointB.y - pointA.y));
        double ed = Math.hypot(dx,dy);
        return ed;
    }

    // Get the TSPLib graph name
    public String getName() {
		return this.name;
	}

    public String getFileName() {
		return this.filename;
	}

    public int getSize() {
        return this.list.size();
    }

    // Get the position of the start city
    public int getStartCity() {
        return this.startCity;
    }

    // Set the position of the start city
    public void setStartCity(int i) {
        int city = i;
        if (city <= this.list.size() && city != 0) {
            this.startCity = city;
            this.resetTestTour();
        }
        else {
            System.exit(1);
        }
    }

	// Get length of a given tour using the adjacency matrix
	public double findTourLength(ArrayList<Integer> tour) {
		double len = 0;
		double lineWeight;
		int i, j;
		for (i = 0, j = 1; j < tour.size(); ++i, ++j) {
			lineWeight = adjMatrix[tour.get(i) - 1][tour.get(j) - 1];
			len = len + lineWeight;
		}
		lineWeight = adjMatrix[this.startCity - 1][tour.get(tour.size() - 1) - 1];
		len = len + lineWeight;
		return len;
	}

	public ArrayList<Integer> getTestTour() {
		return this.testTour;
	}

    public ArrayList<Integer> getOddDegrees() {
        return this.oddDegrees;
    }

	public double getOptimalTourLength() {
		return this.optimalTourLength;
	}

	public double getApproxTourLength() {
		return this.nnTourLength;
	}

	public double getTreeTourLength() {
		return this.treeTourLength;
	}

	public double getTwiceTourLength() {
		return this.twiceTourLength;
	}

	public double getChrTourLength() {
		return this.chrTourLength;
	}

    public double getTimeTaken() {
        return this.timeTaken;
    }

    public ArrayList<Integer> deepCopyList(ArrayList<Integer> inList) {
		ArrayList<Integer> outList = new ArrayList<Integer>(inList.size());
		for (int i : inList) {
			outList.add(i);
		}
		return outList;
	}

    // Exhaustive search implemented with backtracking and no timewasting//{{{
	public ArrayList<Integer> solveTSP() {
		long startTime = System.nanoTime();
		exhaustiveSearch(this.testTour);
		long endTime = System.nanoTime();
		this.timeTaken = (endTime - startTime) / 1000;
		return this.optimalTour;
	}

    public void exhaustiveSearch(ArrayList<Integer> tour) {
		// Set optimal tour if this tour has completed and is best so far
		if ((this.list.size() == tour.size()) && (findTourLength(tour) <= optimalTourLength)) {
			this.optimalTour = deepCopyList(tour);
			this.optimalTourLength = findTourLength(tour);
			return;
		}
		// Cut recursion if this tour has passed the best so far
		else if (findTourLength(tour) > this.optimalTourLength) {
			return;
		}
		// Backtracking logic
		else {
			for (int city = 1; city <= this.list.size(); ++city) {
				if (!tour.contains(city)) {
					// Add unvisited city to tour
					tour.add(city);
					// Recurse!
					exhaustiveSearch(tour);
					// Remove city when its permutations have been tested
					tour.remove(tour.indexOf(city));
				}
			}
		}
	}
//}}}

    // Nearest Neighbour//{{{
    public ArrayList<Integer> getNNTour() {
		long startTime = System.nanoTime();
        nearestNeighbour();
		long endTime = System.nanoTime();
		this.timeTaken = (endTime - startTime) / 1000;
		return this.nnTour;
	}

    // Nearest Neighbour / greedy algorithm
    public void nearestNeighbour() {
        // Start at first city
        this.nnTour = new ArrayList<Integer>();
        this.nnTour.add(this.startCity);
        double lowestCost;
        int lastCity, currCity;
        while (this.nnTour.size() < this.list.size()) {
            lowestCost = Integer.MAX_VALUE;
            lastCity = this.nnTour.get(this.nnTour.size() - 1);
            currCity = lastCity;
            // Find shortest edge connecting current city and an unvisited city C
            for (int i = 1; i <= this.list.size(); ++i) {
                if (adjMatrix[lastCity - 1][i - 1] < lowestCost && !this.nnTour.contains(i)) {
                    lowestCost = adjMatrix[lastCity - 1][i - 1];
                    currCity = i;
                }
            }
            // Set current city to C and mark C as visited
            this.nnTour.add(currCity);
            // If all cities in problem are visited, then terminate
        }
        this.nnTourLength = findTourLength(nnTour);
    }
//}}}

    public ArrayList<Coordinate<Integer,Integer>> getTreeTour() {
		//long startTime = System.nanoTime();
        minimumSpanningTree();
		//long endTime = System.nanoTime();
		//this.timeTaken = (endTime - startTime) / 1000;
		return this.MST;
	}

    public void minimumSpanningTree() {
        double minimumCost;
        int lastCity, currCity;
        ArrayList<Integer> visitedCities = new ArrayList<Integer>();
        this.MST = new ArrayList<Coordinate<Integer,Integer>>();
        visitedCities.add(this.startCity);
        while (visitedCities.size() < this.list.size()) {
            // Run the algorithm
            minimumCost = Double.MAX_VALUE;
            lastCity = visitedCities.get(visitedCities.size() - 1);
            currCity = lastCity;
            for (int city : visitedCities) {
                // find the cheapest cost to unvisited node
                for (int i = 1; i <= this.list.size(); ++i) {
                    if ((!visitedCities.contains(i)) && (adjMatrix[city - 1][i - 1] < minimumCost)) {
                        minimumCost = adjMatrix[city - 1][i - 1];
                        lastCity = city;
                        currCity = i;
                    }
                }
            }
            visitedCities.add(currCity);
            this.MST.add(new Coordinate<Integer,Integer>(lastCity,currCity));
        }
        this.treeTourLength = getTreeCost(this.MST);
    }

    public double getTreeCost(ArrayList<Coordinate<Integer,Integer>> tree) {
        double treeCost = 0;
        for (Coordinate<Integer,Integer> coord : tree) {
            treeCost = treeCost + adjMatrix[coord.x - 1][coord.y - 1];
        }
        return treeCost;
    }

    // Make the tree!
    //public TreeNode<Integer> generateMST(ArrayList<Coordinate<Integer,Integer>> points) {
        //// Set root of tree
        //minimumSpanningTree = new TreeNode<Integer>(startCity, true);
        //TreeNode<Integer> tempNode;
        //// Loop through the list of coordinates and create a tree using the
        //// x value as the parent and the y value as the child
        //for (Coordinate<Integer,Integer> p : points) {
            //tempNode = minimumSpanningTree.findNode(p.x).addChild(p.y);
        //}
        //return minimumSpanningTree;
    //}

    //public TreeNode<Integer> generateMST(ArrayList<Coordinate<Integer,Integer>> points) {
        //// Set root of tree
        //ArrayList<Integer> roots = findTreeRoot(points);
        //for (int r : roots) {
            //System.out.println("Root: " + r);
        //}
        //int root = roots.get(0);
        //TreeNode<Integer> currentx;
        //TreeNode<Integer> currenty;
        //minimumSpanningTree = new TreeNode<Integer>(root, true);
        //ArrayList<Coordinate<Integer,Integer>> pointsUsed = new ArrayList<Coordinate<Integer,Integer>>();

        ////ArrayList<Integer> usedEdges = new ArrayList<Integer>();
        ////usedEdges.add(root);
        //for (Coordinate<Integer,Integer> p : points) {
            //if (p.y == root) {
                //minimumSpanningTree.findNode(p.y).addChild(p.x);
                //pointsUsed.add(p);
            //}
            //else if (p.x == root) {
                //minimumSpanningTree.findNode(p.x).addChild(p.y);
                //pointsUsed.add(p);
            //}
        //}
        //while (pointsUsed.size() < points.size()) {
            //for (Coordinate<Integer,Integer> p : points) {
                //if (!pointsUsed.contains(p)) {
                    //currentx = minimumSpanningTree.findNode(p.x);
                    //currenty = minimumSpanningTree.findNode(p.y);
                    //if (!(roots.contains(currentx.value) || roots.contains(currenty.value))) {
                        //currenty.addChild(p.x);
                        //pointsUsed.add(p);
                    //}
                    //else if (currentx.value != root && !currentx.childNodes.isEmpty()) {
                        //currentx.addChild(p.y);
                        //pointsUsed.add(p);
                    //}
                //}
            //}
        //}
        ////while (points.size() > 0) {
            ////for (int p = 0; p < points.size(); ++p) {
                ////current = points.get(p);
                ////if (usedEdges.contains(current.x) || usedEdges.contains(current.y)) {
                    ////if (usedEdges.contains(current.x)) {
                        ////minimumSpanningTree.findNode(current.x).addChild(current.y);
                        ////usedEdges.add(current.y);
                    ////}
                    ////else {
                        ////minimumSpanningTree.findNode(current.y).addChild(current.x);
                        ////usedEdges.add(current.x);
                    ////}
                    ////System.out.println("Removing edge: " + points.get(p).x + " " + points.get(p).y);
                    ////points.remove(p);
                ////}
            ////}
        ////}
        //return minimumSpanningTree;
    //}

    public ArrayList<Integer> findTreeRoot(ArrayList<Coordinate<Integer,Integer>> points) {
        int appears;
        ArrayList<Integer> roots = new ArrayList<Integer>();
        for (int i = 1; i <= this.list.size(); ++i) {
            appears = 0;
            for (Coordinate<Integer,Integer> p : points) {
                if (p.x == i)
                    ++appears;
                if (p.y == i)
                    ++appears;
                if (appears > 1)
                    break;
            }
            if (appears == 1)
                roots.add(i);
        }
        return roots;
    }

 //{{{ Twice-Around-The-Tree
    public void treeWalk(TreeNode<Integer> minTree) {
        TreeNode<Integer> currNode = minTree;
        if (currNode.checkRoot()) {
            twiceTour = new ArrayList<Integer>();
            System.out.println("ROOT = " + currNode.value);
            twiceTour.add(currNode.value);
        }
        for (TreeNode<Integer> child : currNode.childNodes) {
            twiceTour.add(child.value);
            System.out.println("ADDED = " + child.value);
            treeWalk(child);
        }
    }

    public ArrayList<Integer> getTwiceTour() {
		long startTime = System.nanoTime();
        Graph g = new Graph();
        ArrayList<Coordinate<Integer,Integer>> mstEdges = getTreeTour();
        for (int v = 1; v <= this.list.size(); ++v)
            g.addVertex(v);
        for (Coordinate<Integer,Integer> e : mstEdges)
            g.addEdge(e);
        this.twiceTour = g.dfsIter(1);
		long endTime = System.nanoTime();
		this.timeTaken = (endTime - startTime) / 1000;
        this.twiceTourLength = findTourLength(this.twiceTour);
		return this.twiceTour;
	}
//}}}

    //public void oddDegreeWalk(TreeNode<Integer> minTree) {
        //// get odd degrees
        //TreeNode<Integer> currNode = minTree;

        //if (currNode.checkRoot()) {
            //oddDegrees = new ArrayList<Integer>();
            //if (currNode.childNodes.size() % 2 != 0)
                //oddDegrees.add(currNode.value);
        //} else if (currNode.childNodes.size() % 2 == 0)
            //oddDegrees.add(currNode.value);

        //if (currNode.childNodes.size() > 0) {
            //for (TreeNode<Integer> child : currNode.childNodes)
                //oddDegreeWalk(child);
        //}
    //}

    public void oddDegreeWalk(ArrayList<Coordinate<Integer,Integer>> edgesIn) {
        oddDegrees = new ArrayList<Integer>();
        int count;
        for (int i = 0; i < list.size(); ++i) {
            count = 0;
            for (Coordinate<Integer,Integer> cd : edgesIn) {
                if ((cd.x == (i+1)) || (cd.y == (i+1))) {
                    ++count;
                }
            }
            if (count % 2 != 0)
                oddDegrees.add(i+1);
        }
    }

    // Greedy approximation algorithm for perfect matching
    public ArrayList<Coordinate<Integer,Integer>> getMatching(ArrayList<Integer> odds) {
        ArrayList<Coordinate<Integer,Integer>> matching = new ArrayList<Coordinate<Integer,Integer>>();
        ArrayList<Integer> toDo = deepCopyList(odds);
        double cost;
        int x = 0, y = 0, ind = 0;
        // get Matching edges
        while (toDo.size() > 0) {
            cost = Double.MAX_VALUE;
            for (int i = 1; i < toDo.size(); ++i) {
                if (adjMatrix[toDo.get(0) - 1][toDo.get(i) - 1] < cost) {
                    x = toDo.get(0);
                    y = toDo.get(i);
                    ind = i;
                }
            }
            matching.add(new Coordinate<Integer,Integer>(x,y));
            toDo.remove(ind);
            toDo.remove(0);
        }
        return matching;
    }

    public ArrayList<Coordinate<Integer,Integer>> getMultigraph(ArrayList<Coordinate<Integer,Integer>> minT, ArrayList<Coordinate<Integer,Integer>> match) {
        ArrayList<Coordinate<Integer,Integer>> tempList = new ArrayList<Coordinate<Integer,Integer>>(minT);
        tempList.addAll(match);
        return tempList;
    }

    public ArrayList<Integer> getChrTour() {
        long startTime = System.nanoTime();
        // Christofides
        minimumSpanningTree();
        oddDegreeWalk(MST);
        ArrayList<Coordinate<Integer,Integer>> matchEdges = getMatching(getOddDegrees());
        ArrayList<Coordinate<Integer,Integer>> mGraph = getMultigraph(MST, matchEdges);
        System.out.println("Multigraph: ");
        for (Coordinate<Integer,Integer> e : mGraph) {
            System.out.print("[" + e.x + "," + e.y + "] ");
        }
        EulerianCircuit eCircuit = new EulerianCircuit(mGraph, this.list.size(), this.startCity);
        this.chrTour = eCircuit.getEulerianCircuit();
        long endTime = System.nanoTime();
        this.timeTaken = (endTime - startTime) / 1000;
        this.chrTourLength = findTourLength(this.chrTour);
        return this.chrTour;
    }

}
