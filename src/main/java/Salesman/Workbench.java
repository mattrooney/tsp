package Salesman;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.ArrayList;
import net.miginfocom.swing.*;

public class Workbench extends JFrame {

    private static final long serialVersionUID = 1L;

    // Panels
    private JPanel panel = new JPanel(new MigLayout());
    // Left side
    private JPanel leftPanel = new JPanel(new GridLayout(2,1));
    private JPanel graphPanel = new JPanel();
    private JPanel leftLowPanel = new JPanel(new GridLayout(2,1));
    private JPanel buttonPanel = new JPanel(new MigLayout("center"));
    private JPanel bottomPanel = new JPanel(new MigLayout("center"));
    // Right side
    private JPanel rightAlgoPanel = new JPanel(new MigLayout("", "", ""));
    private JPanel rightTourPanel = new JPanel(new MigLayout("left"));
    private JPanel rightPanel = new JPanel(new MigLayout("center"));

    private static final String[] AXES = { "City", "X-coord", "Y-coord" };
    private static final String[] COLUMNS = { "Algorithm", "Tour cost", "Time taken (s)", "Ratio of optimal" };
    private String[][] algoTableData  = {
        { "Optimal tour", null, null, "1" },
        { "Nearest neighbour", null, null, null },
        { "Twice-around-the-tree", null, null, null },
        { "Christofides", null, null, null }
    };
    private int width = 900;
    private int height = 600;
    private File inFile;
    private TSPGraph graph;
    private String[][] coords;
    private JTable table;
    private JTable algoTable;
    private JScrollPane scroller;
    private JScrollPane algoScroller;
    private JScrollPane tourScroller;
    private JLabel name;
    private JTextArea tourText;
    private Font font;

    private int graphCities = 0;
    private double ratio = 0.0;

    private ArrayList<Integer> optTour;
    private ArrayList<Integer> nnTour;
    private ArrayList<Integer> twiceTour;
    private ArrayList<Integer> chrTour;

    // Buttons
    private JButton loadSmallButton = new JButton("Choose small graph...");
    private JButton loadLargeButton = new JButton("Choose large graph...");
    private JButton quitButton = new JButton("Quit");
    private JButton esButton = new JButton("Exhaustive search");
    private JButton nnButton = new JButton("Nearest neighbour");
    private JButton twiceButton = new JButton("Twice-around-the-tree");
    private JButton chrButton = new JButton("Christofides' algorithm");

    public Workbench() {
        initUI();
    }

    // Allow setting width / height?
    public Workbench(int x, int y) {
        width = x;
        height = y;
        initUI();
    }

    // Return the chosen file
    public File getFile() {
        return inFile;
    }

    // Draw
    public final void initUI() {
        name = new JLabel("Select graph to begin");
        name.setAlignmentX(Component.CENTER_ALIGNMENT);
        scroller = new JScrollPane();
        algoScroller = new JScrollPane();
        font = new Font("Verdana", Font.BOLD, 12);
        tourText = new JTextArea("Calculated tour will appear here");
        tourText.setFont(font);
        tourText.setLineWrap(true);
        tourScroller = new JScrollPane(tourText);
        tourScroller.setVisible(true);

        // Add actions to buttons
        loadSmallButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                final JFileChooser fChooser = new JFileChooser();
                fChooser.setCurrentDirectory(new File("../../resources/main/graph_data/small/"));
                int option = fChooser.showOpenDialog(Workbench.this);
                if(option == JFileChooser.APPROVE_OPTION) {
                    name.setText("Loading graph...");
                    tourText.setText("Calculated tour will appear here");
                    setEnabled(false);
                    graphPanel.remove(scroller);
                    rightAlgoPanel.remove(algoScroller);
                    clearAlgoTable();
                    getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                    Thread worker = new Thread() {
                        public void run() {
                            inFile = fChooser.getSelectedFile();

                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    updateGraph();
                                    getContentPane().setCursor(Cursor.getDefaultCursor());
                                    buttonPanel.setVisible(true);
                                    setEnabled(true);
                                }
                            });
                        }
                    };

                    worker.start();
                }
            }
        });

        loadLargeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                final JFileChooser fChooser = new JFileChooser();
                fChooser.setCurrentDirectory(new File("../../resources/main/graph_data/large/"));
                int option = fChooser.showOpenDialog(Workbench.this);
                if(option == JFileChooser.APPROVE_OPTION) {
                    name.setText("Loading graph...");
                    tourText.setText("Calculated tour will appear here");
                    setEnabled(false);
                    graphPanel.remove(scroller);
                    rightAlgoPanel.remove(algoScroller);
                    clearAlgoTable();
                    getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                    Thread worker = new Thread() {
                        public void run() {
                            inFile = fChooser.getSelectedFile();

                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    updateGraph();
                                    getContentPane().setCursor(Cursor.getDefaultCursor());
                                    buttonPanel.setVisible(true);
                                    setEnabled(true);
                                }
                            });
                        }
                    };

                    worker.start();
                }
            }
        });

        quitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });

        esButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                setEnabled(false);
                getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                Thread worker = new Thread() {
                    public void run() {
                        optTour = graph.solveTSP();

                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                setOptTourData();
                                updateAlgoTable();
                                tourText.setText(getTourString(optTour));
                                getContentPane().setCursor(Cursor.getDefaultCursor());
                                setEnabled(true);
                            }
                        });
                    }
                };

                worker.start();
            }
        });

        nnButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                setEnabled(false);
                getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                Thread worker = new Thread() {
                    public void run() {
                        nnTour = graph.getNNTour();

                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                setNNTourData();
                                updateAlgoTable();
                                tourText.setText(getTourString(nnTour));
                                getContentPane().setCursor(Cursor.getDefaultCursor());
                                setEnabled(true);
                            }
                        });
                    }
                };

                worker.start();
            }
        });

        twiceButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                setEnabled(false);
                getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                Thread worker = new Thread() {
                    public void run() {
                        twiceTour = graph.getTwiceTour();

                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                setTwiceTourData();
                                updateAlgoTable();
                                tourText.setText(getTourString(twiceTour));
                                getContentPane().setCursor(Cursor.getDefaultCursor());
                                setEnabled(true);
                            }
                        });
                    }
                };

                worker.start();
            }
        });

        chrButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                setEnabled(false);
                getContentPane().setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

                Thread worker = new Thread() {
                    public void run() {
                        chrTour = graph.getChrTour();

                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                setChrTourData();
                                updateAlgoTable();
                                tourText.setText(getTourString(chrTour));
                                getContentPane().setCursor(Cursor.getDefaultCursor());
                                setEnabled(true);
                            }
                        });
                    }
                };

                worker.start();
            }
        });
        // Add problem name
        graphPanel.setLayout(new BoxLayout(graphPanel, BoxLayout.PAGE_AXIS));
        graphPanel.add(name);
        graphPanel.add(new JSeparator(SwingConstants.HORIZONTAL));



        // Add buttons to panel
        Dimension buttonSize = new Dimension(200,30);
        esButton.setPreferredSize(buttonSize);
        nnButton.setPreferredSize(buttonSize);
        twiceButton.setPreferredSize(buttonSize);
        chrButton.setPreferredSize(buttonSize);
        buttonPanel.add(esButton, "wrap");
        buttonPanel.add(nnButton, "wrap");
        buttonPanel.add(twiceButton, "wrap");
        buttonPanel.add(chrButton, "wrap");
        buttonPanel.setVisible(false);

        bottomPanel.add(loadSmallButton, "wrap");
        bottomPanel.add(loadLargeButton, "wrap");
        bottomPanel.add(quitButton, "center, wrap");

        leftPanel.add(graphPanel);
        leftPanel.add(leftLowPanel);
        leftLowPanel.add(buttonPanel);
        leftLowPanel.add(bottomPanel);

        rightTourPanel.add(tourScroller, "w 93%!, h 100%!, left");


        rightPanel.add(rightTourPanel, "w 100%!, h 70%!, wrap");
        rightPanel.add(rightAlgoPanel, "w 100%!, h 30%!");

        // leftPanel
        panel.add(leftPanel, "w 40%!, h 100%!");

        panel.add(new JSeparator(SwingConstants.VERTICAL), "w 1%!, h 100%!");

        // rightPanel
        panel.add(rightPanel, "w 59%!, h 100%!, center");

        getContentPane().add(panel);

        // Set frame attributes
        setTitle("TSP Workbench");
        setSize(width, height);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void updateGraph() {
        // Update the table of coordinates
        graph = new TSPGraph(inFile);
        coords = graph.getCoords();
        graphCities = coords.length;
        graph.createMatrix();
        name.setText(graph.getName());
        table = new JTable(coords, AXES);
        algoTable = new JTable(algoTableData, COLUMNS);
        graphPanel.remove(scroller);
        scroller = new JScrollPane(table);
        scroller.setMaximumSize(new Dimension((int)(width * .4),(int)(height * .4)));
        scroller.setAlignmentX(Component.CENTER_ALIGNMENT);
        graphPanel.add(scroller);
        algoScroller = new JScrollPane(algoTable);
        algoScroller.setBorder(BorderFactory.createEmptyBorder());
        rightAlgoPanel.add(algoScroller, "w 95%!, left");
    }

    public void updateAlgoTable() {
        // Update the table comparing the algos
        rightAlgoPanel.remove(algoScroller);
        algoTable = new JTable(algoTableData, COLUMNS);
        rightAlgoPanel.add(algoScroller, "w 95%!, left");
    }

    public void setRatio(int row) {
        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new FileReader("../../resources/main/graph_data/optimals/optimals"));
            if (!algoTableData[0][1].equals("")) {
                algoTableData[row][3] = String.format("%.4f", Double.parseDouble(algoTableData[row][1]) / graph.getOptimalTourLength());
            }
            else {
                while (br.readLine() != null) {
                    line = br.readLine();
                    if (graph.getFileName().equals(line.split(" ")[0])) {
                        ratio = Double.parseDouble(line.split(" ")[line.split(" ").length - 1]);
                        ratio = Double.parseDouble(algoTableData[row][1]) / ratio;
                        algoTableData[row][3] = String.format("%.4f", ratio);
                        break;
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
    }
    

    public void setEnabled(boolean b) {
        if (graphCities < 17)
            esButton.setEnabled(b);
        else
            esButton.setEnabled(false);
        nnButton.setEnabled(b);
        twiceButton.setEnabled(b);
        chrButton.setEnabled(b);
    }

    public String getTourString(ArrayList<Integer> t) {
        StringBuilder tourString = new StringBuilder();
        for (int city : t) {
            tourString.append(String.valueOf(city) + "  >  ");
        }
        tourString.append(String.valueOf(graph.getStartCity()));
        return tourString.toString();
    }

    public void setOptTourData() {
        // set the opt tour data in table
        algoTableData[0][1] = String.format("%.4f", graph.getOptimalTourLength());
        //algoTableData[0][1] = String.format("%.4f", String.valueOf(graph.getOptimalTourLength()));
        algoTableData[0][2] = String.format("%.4f", graph.getTimeTaken() / 1000000);
    }

    public void setNNTourData() {
        // set the nearest neighbour tour data in table
        algoTableData[1][1] = String.format("%.4f", graph.getApproxTourLength());
        algoTableData[1][2] = String.format("%.4f", graph.getTimeTaken() / 1000000);
        setRatio(1);
    }

    public void setTwiceTourData() {
        // set the twice-around-the-tree tour data in table
        algoTableData[2][1] = String.format("%.4f", graph.getTwiceTourLength());
        algoTableData[2][2] = String.format("%.4f", graph.getTimeTaken() / 1000000);
        setRatio(2);
    }

    public void setChrTourData() {
        // set the christofides tour data in table
        algoTableData[3][1] = String.format("%.4f", graph.getChrTourLength());
        algoTableData[3][2] = String.format("%.4f", graph.getTimeTaken() / 1000000);
        setRatio(3);
    }

    public void clearAlgoTable() {
        for (int i = 1; i < 4; ++i) {
            algoTableData[i][1] = "";
            algoTableData[i][2] = "";
            algoTableData[i][3] = "";
        }
        algoTableData[0][1] = "";
        algoTableData[0][2] = "";
    }

}
