package Salesman;

import java.util.Stack;
import java.util.ArrayList;
import java.util.LinkedHashSet;

public class EulerianCircuit {

    Stack<Integer> tmpPath;
    Stack<Integer> tmpPathBack;
    ArrayList<Integer> eCircuit;
    int numNodes, startNode;
    int[] nodes;
    int[][] edgeMatrix;

    public EulerianCircuit(ArrayList<Coordinate<Integer,Integer>> edgesIn, int numNodes, int startNode) {
        this.numNodes = numNodes;
        this.startNode = startNode;
        this.nodes = new int[numNodes];
        this.buildEdgeMatrix(edgesIn);
        this.printAllDegrees();
    }

    public ArrayList<Integer> getEulerianCircuit() {
        findTour();
        this.eCircuit = new ArrayList<Integer>(new LinkedHashSet<Integer>(this.eCircuit));
        this.printAllDegrees();
        return this.eCircuit;
    }

    private void buildEdgeMatrix(ArrayList<Coordinate<Integer,Integer>> edgesIn) {
        edgeMatrix = new int[numNodes][numNodes];
        for (Coordinate<Integer,Integer> e : edgesIn) {
            ++edgeMatrix[e.x - 1][e.y - 1];
            ++edgeMatrix[e.y - 1][e.x - 1];
        }
    }

    private boolean visitedAll(int node) {
        for (int i = 0; i < numNodes; ++i) {
            if (edgeMatrix[node-1][i] > 0) {
                return false;
            }
        }
        return true;
    }


    private int getDegree(int node) {
        int degree = 0;
        for (int i = 0; i < numNodes; ++i) {
            degree = degree + edgeMatrix[node-1][i];
        }
        return degree;
    }

    private void printAllDegrees() {
        for (int i = 0; i < numNodes; ++i)
            System.out.println("Degree of " + (i+1) + " = " + getDegree(i+1));
    }

    private void findTour() {
        this.tmpPath = new Stack<Integer>();
        this.tmpPathBack = new Stack<Integer>();
        int current, last;
        int popped = 0;
        eCircuit = new ArrayList<Integer>();
        tmpPath.clear();
        tmpPath.push(startNode);

        while (!tmpPath.empty()) {
            current = tmpPath.peek();
            if (!visitedAll(current))
                tmpPath.push(getNextVertex(current));
            else
                eCircuit.add(tmpPath.pop());
        }
    }

    private int getNextVertex(int currVertex) {
        for (int i = 0; i < numNodes; ++i) {
            if (edgeMatrix[currVertex-1][i] > 0) {
                --edgeMatrix[currVertex-1][i];
                --edgeMatrix[i][currVertex-1];
                return (i+1);
            }
        }
        return 0;
    }
}
