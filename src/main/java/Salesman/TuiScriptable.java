/*
 * This is a simple text-based UI for running various test commands
 * Scriptable
 */
package Salesman;

import java.io.*;
import java.util.ArrayList;
public class TuiScriptable {

    public static void main(String[] args) {
        String s = args[0];

        // Create new TSPGraph from the file
        TSPGraph test = new TSPGraph(new File(s));

        int i = Integer.parseInt(args[1]);
        test.setStartCity(i);

        // Create an adjacency matrix from the cities and their distance from
        // each other
        test.createMatrix();
        //test.printMatrix("matrixoutfile");

        //// Solve problem (using exhaustive search algorithm)
        ArrayList<Integer> optimalTour = new ArrayList<Integer>();
        if (test.getSize() < 15) {
            optimalTour = test.solveTSP();

            // print the optimal tour, its cost and time taken
            System.out.println("\noptimal tour = " + optimalTour);
            System.out.println("tour cost = " + test.getOptimalTourLength());
            System.out.println("time taken = " + test.getTimeTaken() + "\u03bc" + "s");
        }

        ArrayList<Integer> nnTour = new ArrayList<Integer>();
        nnTour = test.getNNTour();

        // Print the Nearest Neighbour tour, its cost and time taken
        System.out.println("\nNearest Neighbour tour = " + nnTour);
        System.out.println("Tour cost = " + test.getApproxTourLength());
        System.out.println("Time taken = " + test.getTimeTaken() + "\u03BC" + "s");

        // Minimum Spanning Tree stuff
        ArrayList<Coordinate<Integer,Integer>> mstTour = new ArrayList<Coordinate<Integer,Integer>>();
        mstTour = test.getTreeTour();
        //Print the Minimum Spanning Tree tour, its cost and time taken
        System.out.print("\nMinimum Spanning Tree tour = ");
        for (Coordinate<Integer,Integer> c : mstTour) {
            System.out.print("(" + c.x + "," + c.y + ") ");
        }
        System.out.println();
        System.out.println("Tour cost = " + test.getTreeTourLength());
        System.out.println("Time taken = " + test.getTimeTaken() + "\u03BC" + "s");

        //TreeNode<Integer> miniTour = test.generateMST(mstTour);
        //miniTour.depthFirstPrint();

        ArrayList<Integer> twiceTour = test.getTwiceTour();

        // Print the Twice-Around-The-Tree tour, its cost and time taken
        System.out.println("\nTwice-Around-The-Tree tour = " + twiceTour);
        System.out.println("Twice cost = " + test.getTwiceTourLength());
        System.out.println("Time taken = " + test.getTimeTaken() + "\u03BC" + "s");

        //miniTour.depthFirstPrint();
        test.oddDegreeWalk(mstTour);
        ArrayList<Integer> odds = test.getOddDegrees();
        System.out.println("Vertices with odd degree: " + odds);
        System.out.println(odds.size() + "/" + test.getSize());

        //ArrayList<Coordinate<Integer,Integer>> pMatching = test.getMatching(odds);
        //System.out.print("Perfect matching: ");
        //for (Coordinate<Integer,Integer> c : pMatching) {
            //System.out.print("[" + c.x + "," + c.y + "] ");
        //}
        System.out.println();

        ArrayList<Integer> chrTour = test.getChrTour();
        

        System.out.println("\nChr tour = " + chrTour);
        System.out.println("Chr cost = " + test.getChrTourLength());
        System.out.println("Time taken = " + test.getTimeTaken() + "\u03BC" + "s");

        //if (chrTour.size() != twiceTour.size())
           //System.out.println("Tour incorrect size!!");
    }
}
