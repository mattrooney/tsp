package Salesman;

import java.util.*;

public class Graph {
    
    enum Colour {
        WHITE, BLACK
    }

    ArrayList<Coordinate<Integer,Integer>> edges = new ArrayList<Coordinate<Integer,Integer>>();
    HashMap<Coordinate<Integer,Integer>, Colour> colourMap = new HashMap<Coordinate<Integer,Integer>, Colour>();

    ArrayList<Integer> nodes = new ArrayList<Integer>();
    HashMap<Integer, Colour> vertexColourmap = new HashMap<Integer, Colour>();

    // Empty graph
    public Graph() {}

    public void addEdge(Coordinate<Integer,Integer> e) {
        edges.add(e);
    }

    public void addVertex(Integer v) {
        nodes.add(v);
    }

    //public ArrayList<Integer> dfsWalk(Coordinate<Integer,Integer> root) {
        //nodes = new ArrayList<Coordinate<Integer,Integer>>();
        ////Initialise vertices
        //for (Coordinate<Integer,Integer> e : edges)
            //colourMap.put(e, Colour.WHITE);
        //dfsVisit(root);

        //ArrayList<Integer> ans = new ArrayList<Integer>();
        //// Nodes now contains edges in dfs walk
        //for (Coordinate<Integer,Integer> e : nodes) {
            //if (!ans.contains(e.x))
                //ans.add(e.x);
            //if (!ans.contains(e.y))
                //ans.add(e.y);
        //}
        //return ans;
    //}

    //public ArrayList<Coordinate<Integer,Integer>> adjacentEdges(Coordinate<Integer,Integer> u) {
        //ArrayList<Coordinate<Integer,Integer>> adj = new ArrayList<Coordinate<Integer,Integer>>();
        //for (Coordinate<Integer,Integer> e : edges) {
            //if (e.isAdjacent(u))
                //adj.add(e);
        //}
        //return adj;
    //}
    
    public ArrayList<Integer> adjacentNodes(Integer node) {
        ArrayList<Integer> adj = new ArrayList<Integer>();
        for (Coordinate<Integer,Integer> e : edges) {
            if (e.x == node)
                adj.add(e.y);
            if (e.y == node)
                adj.add(e.x);
        }
        return adj;
    }

    //public void dfsVisit(Coordinate<Integer,Integer> u) {
        //nodes.add(u);
        //colourMap.put(u, Colour.BLACK);
        //ArrayList<Coordinate<Integer,Integer>> adj = adjacentEdges(u);
        //for (Coordinate<Integer,Integer> v : adj) {
            //if (colourMap.get(v) == Colour.WHITE)
                //dfsVisit(v);
        //}
    //}
    
    public ArrayList<Integer> dfsIter(Integer u) {
        ArrayList<Integer> tour = new ArrayList<Integer>();
        for (Integer e : nodes)
            vertexColourmap.put(e, Colour.WHITE);

        Deque<Integer> stack = new ArrayDeque<Integer>();
        stack.push(u);

        while (!stack.isEmpty()) {
            Integer v = stack.pop();
            if (vertexColourmap.get(v) == Colour.WHITE) {
                vertexColourmap.put(v, Colour.BLACK);

                tour.add(v);

                for (Integer w : adjacentNodes(v))
                    stack.push(w);
            }
        }
        return tour;
    }


}
