package Salesman;

public class Coordinate<X extends Number, Y extends Number> {

    public final X x;
    public final Y y;

    public Coordinate(X x, Y y) {
        this.x = x;
        this.y = y;
    }

    public Boolean isAdjacent(Coordinate other) {
        if (other.x == this.x || other.x == this.y || other.y == this.x || other.y == this.y)
            return true;
        else
            return false;
    }
}
